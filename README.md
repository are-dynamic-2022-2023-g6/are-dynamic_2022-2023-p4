# demo-website-example

## Tutorial
To setup your website and its automatic generation when sources are updated :
- clone this repository
- setup the website deployment, see section "Deploy and view your website" of this tutorial : https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html

Estimated time : 10 to 15 min

## Access the demo website
The website (empty for now) is available at https://are-dynamic-2022-2023-g6.gitlab.io/are-dynamic_2022-2023-p4/

## Add content
You can edit the index.html file, and add other html files to populate your website.
